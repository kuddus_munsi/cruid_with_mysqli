<?php
class Database
{
        public $hostName ="localhost";
        public $dbName ="user_db";
        public $dbUser ="root";
        public $dbpassword ="";
        public $link;
        public $error;

        public  function __construct()
        {
            $this->connectDB();
        }

    public  function connectDB(){

        $this->link = new mysqli($this->hostName,$this->dbUser,$this->dbpassword,$this->dbName);
        if(!$this->link){

            $this->error = "connection fail".$this->link->connect_error;
            return false;

        }
    }

  public function select($query){

        $result = $this->link->query($query) or die($this->link->error.__LINE__);
        if($result-> num_rows > 0){

            return $result;
        }
        else{
            return false;
        }

    }

    public function insert($query){

        $insert_row = $this->link->query($query) or die($this->link->error.__LINE__);
        if($insert_row){

            header("Location:index.php?msg=".urlencode("Data hasbeen inseted succesfully"));
            exit();
        }
        else{
            
            die("Error:(".$this->link->errno.")".$this->link->error);
        }

    }

    public function update($query){

        $update_row = $this->link->query($query) or die($this->link->error.__LINE__);
        if($update_row){

            header("Location:index.php?msg=".urlencode("Data hasbeen update succesfully"));
            exit();
        }
        else{
            
            die("Error:(".$this->link->errno.")".$this->link->error);
        }

    }

    public function delete($query){

        $delete_row = $this->link->query($query) or die($this->link->error.__LINE__);
        if($delete_row){

            header("Location:index.php?msg=".urlencode("Data hasbeen deleted succesfully"));
            exit();
        }
        else{
            
            die("Error:(".$this->link->errno.")".$this->link->error);
        }

    }
}